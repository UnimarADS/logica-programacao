# Aprendendo Python

Vou separar por três repositorios meu estudo dessa linguagem. 
O primeiro repositorio vai ser chamado de **Primeiro Modulo**, o segundo **Segundo Modulo** e o terceiro **Terceiro Modulo**.

## Explicando sobre a ideia do repositorio

A ideia de realizar esse repositorio, é de melhorar minha logica de programação e conhecer mais sobre Python. Vou utilizar os videos do Gustavo Guanabara, para realizar execícios em Python. Cada modulo vai ter um README explicando o que deve ser feito nos exercícios. [Link do canal que estou usando para estudar](https://www.youtube.com/user/cursosemvideo).
