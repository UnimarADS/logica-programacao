# Lista de exercicio do primeiro modulo

1. Crie um algoritmo que leia um número e mostre o seu dobro, triplo e raiz quadrada.

2. Escreva um programa que leia um valor em metros e o exiba convertido em Decimetro, Centimetro, Milimetro, Decametro, Hectometro e Quilometro.

3. Fazer a tabuada de um número X.

4. Crie um programa que leia quanto dinheiro uma pessoa tem na carteira e mostre quantos Dólares ela pode comprar. (Considere US$1.00 = R$ 3.27).

5. Faça um programa que leia a largura e a altura de uma parede em metros, calcule a sua área e a quantidade de tinta necessária para pintá-la. Sabendo que cada litro de tinta, pinta uma área de 2m².

6. Faça um algoritmo que leia o preço de um produto e mostre seu novo preço, com 5% de desconto.

7. Faça um algoritmo que leia o salário de um funcionário e mostre seu novo salário, com 15% de aumento.

8. Escreva um programa que converta uma temperatura digitada em C° e converta para °F.

9. Escreve um programa que pergunte a quantidade de Km percorrida por um carro alugado e a quantidade de dias pelos quais ele foi alugado. Calcule o preço a pagar, sabendo que o carro custa R$60 por dia e R$0.15 por Km rodado.