precoDolar = 5.54

valorReal = float(input("Digite o valor em R$ que você deseja converter: "))
valorDolar = float(input("Digite o valor em US$ que você deseja converter: "))

realDolar = valorReal / precoDolar
dolarReal = valorDolar * precoDolar

print("__" * 10)
print(f"R${valorReal} = US${realDolar:.2f} dolares")
print(f"US${valorDolar} = R${dolarReal:.2f}")