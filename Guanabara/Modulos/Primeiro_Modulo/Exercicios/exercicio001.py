numero = int(input("Digite um numero: "))

dobro = numero * 2
triplo = numero * 3
raizQuadrada = numero **(1/2)

print(f"Dobro de {numero:.0f} = {dobro:.0f}\nTriplo de {numero} = {triplo:.0f}\nA raiz quadrade de {numero} = {raizQuadrada:.1f}")