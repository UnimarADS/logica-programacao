salarioAtual = float(input("Qual o salário do funcionário? R$"))
aumentoSalario = (salarioAtual * 15) / 100
novoSalario = salarioAtual + aumentoSalario
print(f"Um funcionário que ganhava R${salarioAtual}, com 15% de aumento, passa a receber R${novoSalario:.2f}")