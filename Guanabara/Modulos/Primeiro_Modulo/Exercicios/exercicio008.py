Celsius = float(input("Digite o valor em Celsius: "))
Fahrenheit = (Celsius * 1.8) + 32

print(f"A temperatura {Celsius}°C corresponde a {Fahrenheit:.1f}°F!")