valorMetro = float(input("Digite um valor em metros: "))

valorDecimetro = valorMetro * 10
valorCentimetro = valorDecimetro * 10
valorMilimetro = valorCentimetro * 10
valorDecametro = valorMetro / 10
valorHectometro = valorDecametro / 10
valorQuilometro = valorHectometro / 10

resultado = [valorMetro,
    valorDecimetro,
    valorCentimetro,
    valorMilimetro,
    valorDecametro,
    valorHectometro,
    valorQuilometro]

indice = 0
for contador in resultado:
    if contador == valorMetro:
        print(f"{resultado[indice]} m")
    elif contador == valorDecimetro:
        print(f"{resultado[indice]} dm")
    elif contador == valorCentimetro:
        print(f"{resultado[indice]} cm")
    elif contador == valorMilimetro:
        print(f"{resultado[indice]} mm")
    elif contador == valorDecametro:
        print(f"{resultado[indice]} dam")
    elif contador == valorHectometro:
        print(f"{resultado[indice]} hm")
    else:  
        print(f"{resultado[indice]} km")  
        break
    indice += 1