ValorProduto = float(input("Qual o preço do produto? R$"))
valorPromocao = (ValorProduto * 5) / 100
novoValor = ValorProduto - valorPromocao
print(f"O produto que custava R${ValorProduto}, na promoção com desconto de 5% vai custar R${novoValor:.2f}")